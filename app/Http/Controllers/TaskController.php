<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class TaskController extends Controller
{
    /**
     *
     * @param Request $request
     * @return \Illuminate\Http\Response|array
     */
    public function index()
    {
        return Task::latest()->filter(['search', 'searchbody'])->paginate(10);
    }

    /**
     * Combine due, description, assigneduser_id, started_at, title
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response|array
     */
    public function store(Request $request)
    {
        $attributes = [];
        $attributes['due'] = $request->due;
        $attributes['description'] = $request->description;
        $attributes['assigneduser_id'] = $request->assigneduser_id;
        $attributes['taskcreator_id'] =  Auth::user()->id;
        $attributes['completed'] = 0;
        $attributes['started_at'] = $request->started_at;
        $attributes['title'] = Str::slug($request->title);
        $attributes['slug'] = Str::slug($request->title);
        $task = Task::create($attributes);

        return response()->json([
            'message' => 'Task created successfully',
            'task' => $task,
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = Task::find($id);

        if (!$task) {
            return response()->json([
                'message' => 'Task not found',
            ], 404);
        }

        return $task;
    }

    public function edit($id)
    {
        $task = Task::find($id);
        if (!$task) {
            return response()->json([
                'message' => 'Task not found',
            ], 404);
        }

        return response()->json([
            'message' => 'Edit task',
            'task' => $task,
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Task::find($id);

        if (!$task) {
            return response()->json([
                'message' => 'Task not found',
            ], 404);
        }

        $attributes = [];
        $attributes['due'] = $request->due;
        $attributes['description'] = $request->description;
        $attributes['assigneduser_id'] = $request->assigneduser_id;
        $attributes['taskcreator_id'] =  Auth::user()->id;
        $attributes['completed'] = 0;
        $attributes['started_at'] = $request->started_at;
        $attributes['title'] = $request->title;
        $attributes['slug'] = Str::slug($request->title);
        $task->update($attributes);

        return response()->json([
            'message' => 'Task updated successfully',
            'task' => $task,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::find($id);

        if (!$task) {
            return response()->json([
                'message' => 'Task not found',
            ], 404);
        }

        $task->delete();

        return response()->json([
            'message' => 'Task deleted successfully',
        ]);
    }

    public function validateTask(Request $request)
    {
        $attributes = $request->validate([
            'title' => 'required',
            'due' => 'required',
            'description' => 'required',
            'assigneduser_id' => ['required', Rule::exists('users', 'id')]
        ]);

        return $attributes;
    }

    public function completed($id)
    {
        $task = Task::find($id);
        $task->completed = 1;
        $task->update();
        $users = User::where('id', $task->assigneduser_id )
                        ->orWhere('id',$task->taskcreator_id)
                        ->get();
        return response()->json([
            'message' => 'Task marked completed',
            'users'   => $users
        ]);
    }

    public function inprogress($id)
    {
        $task = Task::find($id);
        $task->completed = 2;
        $task->update();
        $users = User::where('id', $task->assigneduser_id )
            ->orWhere('id',$task->taskcreator_id)
            ->get();
        return response()->json([
            'message' => 'Task marked inprogress',
            'users'   => $users
        ]);
    }

}
