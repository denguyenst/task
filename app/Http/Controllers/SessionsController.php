<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class SessionsController extends Controller
{
    public function store(Request $request)
    {
        $attributes['password'] = Hash::make($request->request->get('password'));
        $attributes['username'] = ucwords($request->request->get('username'));
        $attributes['name'] = ucwords($request->request->get('name'));
        $attributes['email'] = ucwords($request->request->get('email'));
        $user = User::create($attributes);
        $token = $user->createToken('API Token')->accessToken;
        dd($token);
        return response()->json([
            'message' => 'Your account has been created',
            'access_token' => $token,
        ], 201);
    }

    public function createLogin()
    {
        return view('sessions.login');
    }

    public function login(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required']
        ]);

        if (!auth()->attempt($credentials)) {
            throw ValidationException::withMessages([
                'email' => 'Credentials could not be verified',
            ]);
        }

        $token = auth()->user()->createToken('API Token')->plainTextToken;

        return response()->json([
            'message' => 'Welcome Back!',
            'access_token' => $token,
        ]);
    }

    public function destroy()
    {
        auth()->user()->tokens()->delete();

        return response()->json([
            'message' => 'Logged out successfully',
        ]);
    }
}
