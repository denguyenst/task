<?php
/**
 * @SWG\Swagger(
 *     schemes={"http"},
 *     host=API_HOST,
 *     basePath="/",
 *     securityDefinitions={
 *         "Bearer": {
 *             "type": "apiKey",
 *             "name": "Authorization",
 *             "in": "header"
 *         }
 *     },
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Laravel and Swagger",
 *         description="Getting started with Laravel and Swagger",
 *         termsOfService="",
 *         @SWG\Contact(
 *             email="name@example.com"
 *         ),
 *     ),
 * )
 */

/**
 * @SWG\Get(
 *     path="/api/tasks",
 *     description="Return a tasks list",
 *     security={
 *         {"Bearer": {}}
 *     },
 *     @SWG\Response(
 *         response=200,
 *         description="OK",
 *     ),
 *     @SWG\Response(
 *         response=422,
 *         description="Missing Data"
 *     )
 * )
 */

/**
 * @SWG\Post(
 *     path="/api/register",
 *     description="Return a token",
 *     @SWG\Parameter(
 *         name="name",
 *         in="query",
 *         type="string",
 *         description="Your name",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="username",
 *         in="query",
 *         type="string",
 *         description="username",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="email",
 *         in="query",
 *         type="string",
 *         description="email",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="password",
 *         in="query",
 *         type="string",
 *         description="password",
 *         required=true,
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="OK",
 *     ),
 *     @SWG\Response(
 *         response=422,
 *         description="Missing Data"
 *     )
 * )
 */

/**
 * @SWG\Post(
 *     path="/api/login",
 *     description="return a access token",
 *     @SWG\Parameter(
 *         name="email",
 *         in="query",
 *         type="string",
 *         description="email",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="password",
 *         in="query",
 *         type="string",
 *         description="password",
 *         required=true,
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="OK",
 *     ),
 *     @SWG\Response(
 *         response=422,
 *         description="Missing Data"
 *     )
 * )
 */


/**
 * @SWG\Post(
 *     path="/api/tasks",
 *     description="add a task",
 *     security={
 *         {"Bearer": {}}
 *     },
 *     @SWG\Parameter(
 *         name="title",
 *         in="query",
 *         type="string",
 *         description="title",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="due",
 *         in="query",
 *         type="string",
 *         description="due",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="started_at",
 *         in="query",
 *         type="string",
 *         description="started_at",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="description",
 *         in="query",
 *         type="string",
 *         description="description",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="assigneduser_id",
 *         in="query",
 *         type="string",
 *         description="assigneduser_id",
 *         required=true,
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="OK",
 *     ),
 *     @SWG\Response(
 *         response=422,
 *         description="Missing Data"
 *     )
 * )
 */

/**
 * @SWG\Put(
 *     path="/api/tasks",
 *     description="add a task",
 *     security={
 *         {"Bearer": {}}
 *     },
 *     @SWG\Parameter(
 *         name="title",
 *         in="query",
 *         type="string",
 *         description="title",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="due",
 *         in="query",
 *         type="string",
 *         description="due",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="started_at",
 *         in="query",
 *         type="string",
 *         description="started_at",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="description",
 *         in="query",
 *         type="string",
 *         description="description",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="assigneduser_id",
 *         in="query",
 *         type="string",
 *         description="assigneduser_id",
 *         required=true,
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="OK",
 *     ),
 *     @SWG\Response(
 *         response=422,
 *         description="Missing Data"
 *     )
 * )
 */

/**
 * @SWG\Post(
 *     path="api/tasks/1/completed",
 *     description="update a task to completed",
 *     security={
 *         {"Bearer": {}}
 *     },
 *     @SWG\Response(
 *         response=200,
 *         description="OK",
 *     ),
 *     @SWG\Response(
 *         response=422,
 *         description="Missing Data"
 *     )
 * )
 */

/**
 * @SWG\Post(
 *     path="api/tasks/1/inprogress",
 *     description="update status of a task to inprogress",
 *     security={
 *         {"Bearer": {}}
 *     },
 *     @SWG\Response(
 *         response=200,
 *         description="OK",
 *     ),
 *     @SWG\Response(
 *         response=422,
 *         description="Missing Data"
 *     )
 * )
 */

/**
 * @SWG\Delete (
 *     path="api/tasks/10",
 *     description="delete a  task",
 *     security={
 *         {"Bearer": {}}
 *     },
 *     @SWG\Response(
 *         response=200,
 *         description="OK",
 *     ),
 *     @SWG\Response(
 *         response=422,
 *         description="Missing Data"
 *     )
 * )
 */
