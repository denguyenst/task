<?php

use App\Http\Controllers\CommentController;
use App\Http\Controllers\SessionsController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('api')->group(function () {
    Route::post('/register', [SessionsController::class, 'store']);
    Route::post('/login', [SessionsController::class, 'login'])->name('sessions.login');
});
Route::middleware(['auth'])->group(function () {
    Route::prefix('api')->group(function () {
        Route::get('/tasks', [TaskController::class, 'index']);
        Route::post('/tasks', [TaskController::class, 'store'])->name('tasks.store');
        Route::get('/tasks/{id}', [TaskController::class, 'show'])->name('tasks.show');;
        Route::put('/tasks/{id}', [TaskController::class, 'update'])->name('tasks.update');
        Route::delete('/tasks/{id}', [TaskController::class, 'destroy'])->name('tasks.destroy');
        Route::post('/tasks/{id}/completed', [TaskController::class, 'completed'])->name('tasks.completed');
        Route::post('/tasks/{id}/inprogress', [TaskController::class, 'inprogress'])->name('tasks.inprogress');
    });
});
